# TaxIt

Please find the TaxIt repository at [TaxIt](https://gitlab.com/mkuhring/TaxIt).

TaxIt is an iterative and automated computational pipeline for untargeted strain
level identification of microbial tandem MS spectra.
